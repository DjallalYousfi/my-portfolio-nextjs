import React from "react";
import Link from "next/link";
import "./Header.modules.css";

function Header() {
  return (
    <nav>
      <header className="header">
        <ul className="header-list">
          <li className="nom">Djallal Yousfi</li>
        </ul>
        <ul className="header-list">
          <li>
            <Link href="/">Home</Link>
          </li>
          <li>
            <Link href="/AboutMe">About</Link>
          </li>
          <li>
            <Link href="/Formation">Formation</Link>
          </li>
          <li>
            <Link href="/Projects">Projects</Link>
          </li>
          <li>
            <Link href="/Contact">Contact</Link>
          </li>
          <li>
            <Link href="/Testimonials">Testimonials</Link>
          </li>
        </ul>
      </header>
    </nav>
  );
}

export default Header;
