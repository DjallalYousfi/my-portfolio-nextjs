"use client";
import React from "react";
import "./Projects.css";
import Image from "next/image";
import image from "../Images/imageProj.png";
import Link from "next/link";

function Projects() {
  return (
    <div className="projects">
      <h2>Projects</h2>
      <div className="projets-12">
        <div className="projet1">
          <ul>
            <li>
              <Image src={image} alt="Projet1" className="img-projet1" />
              <p>2020</p>
              <h2>Application web E-learning</h2>
            </li>
            <li>
              <Link href="/FirstProject">
                <button className="btnseemore1">See More ...</button>
              </Link>
            </li>
          </ul>
        </div>

        <div className="projet2">
          <ul>
            <li>
              <Image src={image} alt="Projet1" className="img-projet1" />
              <p>2023</p>
              <h2>
                Mise en place d'une plateforme d'envoi d'objets à l'étranger{" "}
                <br />
                Co-Luggage
              </h2>
            </li>
            <li>
              <Link href="/SecondProject">
                <button className="btnseemore1">See More ...</button>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Projects;
